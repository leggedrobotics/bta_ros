bta_ros (Argos3D sensor)
===================
### ROS integration for Bluetechnix devices operated by the Bluetechnix 'Time of Flight' BtaTofApi library. ###

# Summary #

This driver allows to configure your system and ROS to use all Bluetechnix Time of Flight cameras supported by the BltToFApi. It includes an example allowing you to visualize depth data using the rviz viewer included in ROS. It shows you how to use the ToF devices together with ROS and how you can modify operating parameters. Besides the ToF data, we have included a nodelet to capture rgb video from those devices that include a 2D sensor, such as the Argos 3D P320 or Sentis TOF P510. 

To read the docummentation of bta_ros, please visit the wiki page on ROS: http://wiki.ros.org/bta_ros

## Install

    apt-get install ros-indigo-camera-info-manager ros-indigo-pcl-ros ros-indigo-pcl-conversions ros-indigo-perception-pcl gstreamer1.0-dev libgstreamer-plugins-base1.0-dev
    
    cd ~/git
    git clone git@bitbucket.org:ethz-asl-lr/bta_ros.git
    
    cd ~/git/bta_ros
    ./install.sh
    
    cd ~/catkin_ws/src
    ln -s ~/git/bta_ros
    
    catkin build bta_ros
    
    source ~/catkin_ws/devel/setup.bash
    
    roslaunch bta_ros node_tof_P100.launch